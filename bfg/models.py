import sqlalchemy as sa
import sqlalchemy.orm as orm
import sqlalchemy.ext.declarative as declarative

from sqlalchemy.orm import relationship
from pyramid.security import Allow, Everyone
from zope.sqlalchemy import ZopeTransactionExtension as ZTE
from datetime import datetime

DBSession = orm.scoped_session(orm.sessionmaker(extension=ZTE()))
Base = declarative.declarative_base()


class Requests(Base):
    __tablename__ = 'requests'
    id = sa.Column(sa.Integer, primary_key=True)
    ts = sa.Column(sa.DateTime, default=datetime.now())
    title = sa.Column(sa.Text, unique=True)
    # body = Column(Text)


class RequestsFound(Base):
    __tablename__ = 'requests_found'
    id = sa.Column(sa.Integer, primary_key=True)
    ts = sa.Column(sa.DateTime, default=datetime.now())
    # request_id = sa.Column(sa.ForeignKey('requests.id'), nullable=False)
    request_id = sa.Column(sa.ForeignKey('requests.id'), nullable=True)
    request = relationship("Requests", backref="requests_found")


    title = sa.Column(sa.Text, unique=True)
    body = sa.Column(sa.Text)


class Root(object):
    __acl__ = [(Allow, Everyone, 'view'),
               (Allow, 'group:editors', 'edit')]

    def __init__(self, request):
        pass