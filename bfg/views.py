# -*- coding: utf-8 -*-
import requests
import math
import random
import json
from pyramid.view import view_config

from .models import *

with open('/opt/bfg/bfg/proxylist.csv', 'r') as proxyH:
    proxy_list = [proxy for proxy in proxyH.read().split('\n')]

@view_config(route_name='new_request', renderer='templates/new_request.jinja2')
def index(request):
    return {}

@view_config(route_name='process_request', renderer='templates/process_request.jinja2')
def process_request(request):
    title = request.POST['title']
    # print(title)

    if title:
        order = 'desc'
        sort = 'activity'
        # proxies = {'http': random.choice(proxy_list)}
        url = 'http://api.stackexchange.com/2.2/search?order=%s&sort=%s&intitle=%s&site=stackoverflow' % (order, sort, title)
        response = requests.get(url)
        # response = requests.get(url, proxies=proxies)
        response_dict = response.json()

        if 'items' in response_dict:
            DBSession.add(Requests(title=title))
            request_id = (DBSession.query(Requests).filter_by(title=title).one()).id
            for row in response_dict['items']:
                print(row['title'])
                DBSession.add(RequestsFound(title=row['title'], request_id=request_id))
            return {'saved': True}
        else:
            return {}
    else:
        return {}


@view_config(route_name='show_requests', renderer='templates/show_requests.jinja2')
def show_requests(request):
    # print(type(request.registry.settings['row_variants']))
    # print(request.registry.settings['row_variants'])

    page = int(request.matchdict['page'])
    rows = int(request.matchdict['rows'])
    sort = request.matchdict['sort']

    if sort == 'desc':
        saved_requests = DBSession.query(Requests).filter_by().order_by(Requests.ts.desc())
    else:
        saved_requests = DBSession.query(Requests).filter_by().order_by(Requests.ts.asc())
    saved_requests_count = saved_requests.count()

    pages = math.ceil(saved_requests_count/rows)
    page_list = [i for i in range(1, pages+1)]

    return {'saved_requests': saved_requests, 'saved_requests_count': saved_requests_count, 'page_list': page_list,
            'cur_page': page, 'cur_rows': rows, 'cur_sort': sort, 'row_variants': eval(request.registry.settings['row_variants']) }


@view_config(route_name='request_results', renderer='templates/request_results.jinja2')
def request_results(request):
    page = int(request.matchdict['page'])
    rows = int(request.matchdict['rows'])
    sort = request.matchdict['sort']
    request_id = request.matchdict['request_id']

    request_title = DBSession.query(Requests).filter_by(id=request_id).one().title
    if sort == 'desc':
        request_results = DBSession.query(RequestsFound).filter_by(request_id=request_id).order_by(RequestsFound.ts.desc())
    else:
        request_results = DBSession.query(RequestsFound).filter_by(request_id=request_id).order_by(RequestsFound.ts.asc())
    request_results_count = request_results.count()
    pages = math.ceil(request_results_count/rows)
    page_list = [i for i in range(1, pages+1)]

    return {'request_results': request_results[(page-1)*rows:page*rows], 'request_title': request_title, 'cur_page': page, 'request_id': request_id,
            'results_count': request_results_count, 'page_list': page_list, 'cur_rows': rows, 'cur_sort': sort, 'row_variants': eval(request.registry.settings['row_variants'])}

