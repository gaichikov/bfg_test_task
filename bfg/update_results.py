import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.httpserver

import random
import requests
import logging
import sys
import time
import threading
import transaction
import json

from datetime import datetime, timedelta

from sqlalchemy import engine_from_config
from zope.sqlalchemy import ZopeTransactionExtension

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from bfg.models import (
    DBSession,
    Requests,
    RequestsFound,
    Base,
    )


config_uri = sys.argv[1]
setup_logging(config_uri)
settings = get_appsettings(config_uri)
engine = engine_from_config(settings, 'sqlalchemy.')
DBSession.configure(bind=engine)
# Base.metadata.create_all(engine)

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='/var/log/update_results.log',
                    filemode='w'
                    )

print(settings['updater_timeout'])

class WebSocketHandler(tornado.websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self, *args):
        logging.info('Client connected')
        print('Client connected')

    def on_close(self):
        logging.info('Client disconnected ')
        print('Client disconnected ')

    def on_message(self, message):
        """ Send updated records on request from web """
        print(message)

        if message == 'refresh_results':
            print('Checking amount of new results')
            try:
                # new_results = DBSession.query(RequestsFound).filter(RequestsFound.ts >= (datetime.now() - timedelta(minutes=5)) , RequestsFound.ts <= datetime.now())
                new_results = DBSession.query(RequestsFound).filter(RequestsFound.ts >= (datetime.now() - timedelta(minutes=5)))
                print(new_results.count())
                results_dict = {}
                if new_results:
                    for result in new_results:
                        if result.request.title not in results_dict:
                            results_dict[result.request.title] = result.request_id
                # self.write_message('Last updated results ' + new_results.count())
                update_dict = {'ts': str(datetime.now()), 'new_results': new_results.count(), 'results_dict': json.dumps(results_dict)}
                self.write_message(json.dumps(update_dict))
            except Exception as e:
                update_dict = {'ts': str(datetime.now()), 'new_results': 0}
                self.write_message(json.dumps(update_dict))
                logging.error('Error getting new results: ' + str(e))
        last_update_ts = datetime.now()


def update_results():
    ''' Make periodical requests to stackexchange '''
    while True:
        saved_requests = DBSession.query(Requests).filter_by()
        for saved_request in saved_requests:
            time.sleep(random.randint(5,20))
            try:
                make_request(saved_request.id, saved_request.title)
            except Exception as e:
                logging.error('Error making request for title %s ' % saved_request.title)
        time.sleep(int(settings['updater_timeout']))


def make_request(request_id, title):
    print('Making request')
    order = 'desc'
    sort = 'creation'
    url = 'http://api.stackexchange.com/2.2/search?order=%s&sort=%s&intitle=%s&site=stackoverflow' % (order, sort, title)
    print(url)
    response = requests.get(url)
    response_dict = response.json()
    for row in response_dict['items']:
        get_row = DBSession.query(RequestsFound).filter_by(title=row['title'])
        # print(get_row)
        if not get_row.count():
            with transaction.manager:
                try:
                    new_row = RequestsFound(title=row['title'], request_id=request_id)
                    DBSession.add(new_row)
                    logging.info('Added new title: '+row['title'])
                except Exception as e:
                    logging.error('Error adding new result row: ' + str(e))
        # else:
        #     print('item exists ' + row['title'])
        #     logging.info('item exists: ' + row['title'])


if __name__ == "__main__":
    print('Starting the daemon')
    app = tornado.web.Application([(r'/', WebSocketHandler),])

    updater_thread = threading.Thread(name='update_results', target=update_results)
    updater_thread.daemon = True
    updater_thread.start()

    # Tornado server, receiving signals from web
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()




