# -*- coding: utf-8 -*-
from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from .models import DBSession, Base


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    config = Configurator(settings=settings, root_factory='bfg.models.Root')
    config.include('pyramid_chameleon')
    config.include('pyramid_jinja2')
    config.include('pyramid_beaker')
    config.add_static_view('static', 'static', cache_max_age=3600)
    # config.add_route('home', '/')
    config.add_route('process_request', '/process_request')
    config.add_route('show_requests', '/show_requests/{page}/{rows}/{sort}')
    config.add_route('request_results', '/request_results/{request_id}/{page}/{rows}/{sort}')
    config.add_route('new_request', '/')
    config.scan()
    return config.make_wsgi_app()


